---
version: 2
type de projet: Projet de bachelor
année scolaire: 2020/2021
titre: Development Setup Usage Monitoring
abréviation: DevSetupMonitor
filières:
  - Télécommunications
  - Informatique
mandants:
  - Sonova SA (Phonak)
nombre d'étudiants: 1
professeurs co-superviseurs:
  - Nicolas Schroeter
mots-clés: [Python, Concurrent programming, FPGA]
langue: [F,E,D]
confidentialité: oui
réalisation: labo
suite: non
nombre d'étudiants: 1
---
```{=tex}
\begin{center}
\includegraphics[width=0.5\textwidth]{image/Phonak.jpg}
\end{center}
```

## Description/Context
In the scope of the development of an ASIC for wireless communication, Sonova (Phonak Communications) is using expensive FPGA setups. The system has to dynamicaly manage an important number of developers compared to HW setups capabilities. An effective sharing of this resource is important for Sonova.
Actually, the HW setups are controlled by a computer running Windows 10 and are accessed remotely via Windows Remote Desktop.
This project aims to increase the effectiveness of sharing setups. Today only a static allocation of a machine to few developers is in place. Then developers have to organize themselves via email, phone calls and so on. This is not ideal since expensive setup are sometimes sleeping while an engineer is seeking one.

## Contraints

* Use developpement process and tools recommanded by Sonova and running on Microsoft OS

* Active monitoring of HW resources

* Human friendly display of collected data. (ie. usage statistics)

* User friendly search of available setup

## Technical environment
* Client side : data collection on windows 10 and in python 3 

* Server side: docker container on ubuntu.

* postgresql

* pandas / dash/plot.ly for display (not mandatory)


## Objectives/Tasks

* Analyze and Describe in details all the interfaces that allow to interact with the FPGA setups

* Depict a detailled resources sharing process with help of Sonova

* Define a well structured architecture that will be able to manage the FPGA HW setups

* Design, develop and validate in real condition the SW application able to manage the FPGA Setup monitoring
