---
version: 2
type de projet: Projet de bachelor
année scolaire: 2020/2021
titre: Apprentissage de la Cryptographie à l'école
abréviation: CryptoLoup
filières:
  - Télécommunications
proposé par étudiant: Chloé Amez-Droz
mandants:
  - Haute école pédagogique (HEP)
nombre d'étudiants: 1
professeurs co-superviseurs:
  - Jacques Supcik
mots-clés: [Didactique, HEP, cryptographie]
langue: [F,E,D]
confidentialité: non
réalisation: labo
suite: oui
attribué à:
  - Chloé Amez-Droz
nombre d'étudiants: 1
---
```{=tex}
\begin{center}
\includegraphics[width=0.5\textwidth]{image/loup.jpg}
\end{center}
```

## Description/Contexte
La Haute École Pédagogique de Fribourg (HEP|PH FR) forme des enseignants pour les classes primaires et secondaires. Les élèves de classes de l’école obligatoire devront, dans un proche avenir, aborder une nouvelle discipline: les sciences informatiques. L’objectif du projet est de développer, avec le support de la HEP|PH FR, un environnement numérique qui permette à des élèves du degré primaire d’acquérir les compétences ciblées en matière de sécurité informatique. Les défis sont certes informatiques, mais aussi didactiques, car l'outil se veut ludique et devra permettre une sensibilisation aux mécanismes de la sécurité des données et à leur confidentialité.

Les compétences ciblées : Crypter, décrypter et hacker un message à l’aide d'une méthode simple (p. ex.: César). Le jeu introduira la notion de clé, de secrets partagés, de confidentialité des échanges et de stratégies d'attaques.

Les travaux préliminaires ont permis de définir les règles de jeu d'une application multi-utilisateur et multisite basée sur le jeu du "loup Garou". Une architecture globale utilisant le cloud a été définie et un premier prototype implémentant des échanges de messages est en cours de réalisation dans le cadre du projet de semestre. Le travail de bachelor se propose de finaliser la conception et de la généraliser pour un usage multi-utilisateur et multisite du jeu. Les interfaces utilisateur (IHM) devront correspondre à une utilisation destinée aux enfants et surtout susciter leur curiosité et leur intérêt. Il se devra d’aboutir sur un produit suffisamment mature et opérationnel et ainsi permettre une validation concrète du jeu avec des groupes d'utilisateurs.

## Contraintes

* Utiliser les concepts pédagogiques discutés avec les étudiants et le superviseur de la HEP|PH FR pour assurer la qualité didactique des scénarios et des activités proposées à travers le jeu.

* Développer un environnement numérique distrué sur un service Cloud qui permette, dans les années à venir, un déploiement élargi (multi utilisateur et multisite) du Jeu.

* L'environnement devra être suffisamment modulaire pour pouvoir accueillir d’autres activités didactiques ciblant d’autres compétences.

* Approche et développement du projet utilisant une approche "Open Source"

* Tenir compte, à tous les niveaux du système, des contraintes de sécurité et de protection des données

## Objectifs/Tâches.

* Concevoir et développer, en collaboration avec les intervenants de la HEP, les interfaces utilisateurs en se basant sur des scénarios d'utilisation du jeu du "loup Garou" élaborés lors du projet de semestre.

* Concevoir dans le détail l'architecture complète du système en se basant sur une solution distribuée de type Cloud

* Déployer et valider complètement la solution logicielle proposée

* Concevoir, développer et valider les scénarii de jeu, toutes les fonctions de chat cryptés ainsi les mécanismes de recherche de clé.

* valider en "situation réelle" les outils développés en jouant les scénarios principaux
